<?php

JHtml::_('jquery.framework');

$debug = config()->get('debug');

doc()->addScriptDeclaration('window.lang = \'' . explode('-', lang()->get('tag'))[0] . '\';');

if ( $debug ) {
    doc()->addScript(rootURL() . trim(NANO_ROOT, '/') . '/resource/vue.js');
    doc()->addScript(rootURL() . trim(NANO_ROOT, '/') . '/resource/es6-promise.min.js');
} else {
    doc()->addScript(rootURL() . trim(NANO_ROOT, '/') . '/resource/vue.min.js');
    doc()->addScript(rootURL() . trim(NANO_ROOT, '/') . '/resource/es6-promise.min.js');
}

$connection = [
    'driver'    => 'mysql',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'database'  => config()->get('db'),
    'username'  => config()->get('user'),
    'password'  => config()->get('password'),
    'prefix'    => config()->get('dbprefix'),
    'host'		=> config()->get('host')
];

if ( preg_match('/^(.*?)\:(.*?\.sock)$/', config()->get('host'), $socketMatch) ) {
    $connection['host'] = $socketMatch[1];
    $connection['unix_socket']  = $socketMatch[2];
}

LaravelArdent\Ardent\Ardent::configureAsExternal($connection, 'en');

Nano\Component\Provider::init();