<?php

namespace Nano\Controller;

use Exception as Exception;
use JFactory as JFactory;

trait RedirectTrait
{

	public function beforeCall($method, $vars)
	{
		if ( preg_match('/^(.*?)(redirect)$/i', $method, $matches) ) {
			JFactory::getApplication()->redirect('index.php?option=com_' . strtolower($this->getNamespace()) . '&view=' . strtolower($this->getClass()) . '&task=' . strtolower($matches[1]));
		}
	}

	protected function getNamespace()
	{
		return current(explode('\\', get_class($this)));
	}

	protected function getClass()
	{
		return end(explode('\\', get_class($this)));
	}

}