<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Select
{
	protected $node;
	protected $value = '';
	protected $options = [];
	protected $optionLabel = 'label';
	protected $optionValue = 'value';

	public static function make()
	{
		return new Select;
	}

	public function __construct()
	{
		$this->node = Node::make('select');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		return $this;
	}

	public function value($value)
	{
		$this->value = $value;
		return $this;
	}

	public function options($options = [], $optionLabel = 'label', $optionValue = 'value')
	{
		$this->options = $options;
		$this->optionLabel = $optionLabel;
		$this->optionValue = $optionValue;

		return $this;
	}

	public function open()
	{
		$html = '';

		$html .= $this->node->open();

		foreach ( toObject($this->options) as $option )
		{
			$node = Node::make('option');

			if ( ( is_string($this->value) || is_numeric($this->value) ) && (string) $option->{$this->optionValue} === (string) $this->value ) {
				$node->selected('selected');
			}

			if ( is_array($this->value) && in_array( (string) $option->{$this->optionValue}, (array) $this->value ) ) {
				$node->selected('selected');
			}

			$html .= $node->value($option->{$this->optionValue})->open() . trans($option->{$this->optionLabel}) . $node->close();
		}

		$html .= $this->node->close();

		return $html;
	}

}