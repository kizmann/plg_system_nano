<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Label
{
	protected $node;
	protected $value = '';
	protected $required = false;

	public static function make()
	{
		return new Label;
	}

	public function __construct()
	{
		$this->node = Node::make('label');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		return $this;
	}

	public function value($value)
	{
		$this->value = $value;
		return $this;
	}

	public function required($required = true)
	{
		$this->required = $required;
		return $this;
	}

	public function open()
	{
		return $this->node->open() . trans($this->value) . ($this->required ? '&nbsp;<span class="label-required">*</span' : '') . $this->node->close();
	}

}