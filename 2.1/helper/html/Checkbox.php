<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Checkbox
{
	protected $node;
	protected $value = [];
	protected $name = '';
	protected $options = [];
	protected $optionLabel = 'label';
	protected $optionValue = 'value';

	public static function make()
	{
		return new Checkbox;
	}

	public function __construct()
	{
		$this->node = Node::make('fieldset');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		return $this;
	}

	public function value($value)
	{
		$this->value = $value;
		return $this;
	}

	public function name($name)
	{
		$this->name = $name . '[]';
		return $this;
	}

	public function options($options = [], $optionLabel = 'label', $optionValue = 'value')
	{
		$this->options = $options;
		$this->optionLabel = $optionLabel;
		$this->optionValue = $optionValue;

		return $this;
	}

	public function open()
	{
		$html = '';

		$html .= $this->node->open();

		// what((array) $this->value);

		foreach ( toObject($this->options) as $index => $option )
		{
			$input = Node::make('input');

			if ( array_key_exists('id', $this->node->attributes) ) {
				$input->id($this->node->attributes['id'] . '_' . $index);
			}

			if ( in_array( (string) $option->{$this->optionValue}, (array) $this->value ) ) {
				$input->checked('checked');
			}

			$label = Node::make('label');

			if ( array_key_exists('id', $this->node->attributes) ) {
				$label->for($this->node->attributes['id'] . '_' . $index);
			}

			$html .= $label->class('checkbox')->open() . $input->type('checkbox')->name($this->name)->value($option->{$this->optionValue})->open() . trans($option->{$this->optionLabel}) . $label->close();
		}

		$html .= $this->node->close();

		return $html;
	}

}