<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Input
{
	protected $node;
	protected $value = '';

	public static function make()
	{
		return new Input;
	}

	public function __construct()
	{
		$this->node = Node::make('input');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		$this->node->type('text');
		return $this;
	}

	public function open()
	{
		return $this->node->open();
	}

}