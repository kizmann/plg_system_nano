<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Form
{
	protected $node;

	public static function make()
	{
		return new Form;
	}

	public function __construct()
	{
		$this->node = Node::make('form');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		$this->node->action($_SERVER['REQUEST_URI'])->method('post');
		return $this;
	}

	public function open()
	{
		return $this->node->open();
	}

	public function close()
	{
		return $this->node->close();
	}

}