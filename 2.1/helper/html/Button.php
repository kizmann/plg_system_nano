<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Button
{
	protected $node;
	protected $value = '';

	public static function make()
	{
		return new Button;
	}

	public function __construct()
	{
		$this->node = Node::make('button');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		$this->node->class('btn');
		return $this;
	}

	public function value($value)
	{
		$this->value = $value;
		return $this;
	}

	public function open()
	{
		return $this->node->open() . trans($this->value) . $this->node->close();
	}

}