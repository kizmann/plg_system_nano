<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Textarea
{
	protected $node;
	protected $value = '';

	public static function make()
	{
		return new Textarea;
	}

	public function __construct()
	{
		$this->node = Node::make('textarea');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		return $this;
	}

	public function value($value)
	{
		$this->value = $value;
		return $this;
	}

	public function open()
	{
		return $this->node->open() . trans($this->value) . $this->node->close();
	}

}