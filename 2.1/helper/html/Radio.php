<?php

namespace Nano\Helper\Html;

use Nano\Helper\Html\Node as Node;

class Radio
{
	protected $node;
	protected $value = '';
	protected $name = '';
	protected $options = [];
	protected $optionLabel = 'label';
	protected $optionValue = 'value';

	public static function make()
	{
		return new Radio;
	}

	public function __construct()
	{
		$this->node = Node::make('fieldset');
		$this->prefill();
	}

	public function __call($attribute, $value)
	{
		$this->node->setAttribute($attribute, current($value));
		return $this;
	}

	public function prefill()
	{
		return $this;
	}

	public function value($value)
	{
		$this->value = $value;
		return $this;
	}

	public function name($name)
	{
		$this->name = $name;
		return $this;
	}

	public function options($options = [], $optionLabel = 'label', $optionValue = 'value')
	{
		$this->options = $options;
		$this->optionLabel = $optionLabel;
		$this->optionValue = $optionValue;

		return $this;
	}

	public function open()
	{
		$html = '';

		$html .= $this->node->open();

		foreach ( toObject($this->options) as $index => $option )
		{
			$input = Node::make('input');

			if ( array_key_exists('id', $this->node->attributes) ) {
				$input->id($this->node->attributes['id'] . '_' . $index);
			}

			if ( (string) $option->{$this->optionValue} === (string) $this->value ) {
				$input->checked('checked');
			}

			$label = Node::make('label');

			if ( array_key_exists('id', $this->node->attributes) ) {
				$label->for($this->node->attributes['id'] . '_' . $index);
			}

			$html .= $label->class('radio')->open() . $input->type('radio')->name($this->name)->value($option->{$this->optionValue})->open() . trans($option->{$this->optionLabel}) . $label->close();
		}

		$html .= $this->node->close();

		return $html;
	}

}