<?php

namespace Nano\Helper\Html;

class Node
{
	public $tag;
	public $attributes = [];

	public static function make($tag = 'div')
	{
		return new Node($tag);
	}

	public function __construct($tag = 'div')
	{
		$this->tag = $tag;
	}

	public function __call($attribute, $value)
	{
		$this->setAttribute($attribute, current($value));
		return $this;
	}

	public function setAttribute($attribute, $value, $search = '_', $replace = '-')
	{
		$this->attributes[str_replace($search, $replace, strtolower($attribute))] = $value;
		return $this;
	}

	public function open()
	{
		$attributeList = '';

		foreach ( $this->attributes as $attribute => $value ) {
			$attributeList .= ' ' . $attribute . '="' . $value . '"';
		}

		return '<' . $this->tag . $attributeList . '>';
	}

	public function close()
	{
		return '</' . $this->tag . '>';
	}

}