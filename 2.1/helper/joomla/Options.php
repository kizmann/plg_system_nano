<?php

namespace Nano\Helper\Joomla;

class Options
{
	public $options = [];

	public function __call($option, $value)
	{
		if( array_key_exists($option, $this->options) ) {
			$this->options[$option] = current($value);
		}

		return $this;
	}

}