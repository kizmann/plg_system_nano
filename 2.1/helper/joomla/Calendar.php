<?php

namespace Nano\Helper\Joomla;

use Nano\Helper\Joomla\Options as Options;
use JHtml as JHtml;

class Calendar extends Options
{
	public $options = [
		'value' 		=> '',
		'name' 			=> '',
		'id' 			=> '',
		'format'		=> '%Y-%m-%d %H:%M:%S',
		'attributes'	=> ''
	];

	public static function make()
	{
		return new Calendar;
	}

	public function render()
	{
		return JHtml::_('calendar', ...array_values($this->options));
	}
}