<?php

namespace Nano\Helper\Joomla;

use Nano\Helper\Joomla\Options as Options;
use JFactory as JFactory;

class Editor extends Options
{
	public $options = [
		'name' 			=> '',
		'value' 		=> '',
		'width' 		=> '100%',
		'height'		=> '400',
		'cols' 			=> '80',
		'rows' 			=> '15',
		'buttons' 		=> true,
		'id' 			=> null,
		'asset' 		=> null,
		'author' 		=> null
	];

	public static function make()
	{
		return new Editor;
	}

	public function render()
	{
		return JFactory::getEditor()->display(...array_values($this->options));
	}
}