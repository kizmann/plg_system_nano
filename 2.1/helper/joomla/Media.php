<?php

namespace Nano\Helper\Joomla;

use Nano\Helper\Joomla\Options as Options;
use JLayoutHelper as JLayoutHelper;

class Media extends Options
{
	public $options = [
		'autocomplete'   	=> '',
	    'autofocus'      	=> false,
	    'class'          	=> '',
	    'description'    	=> '',
	    'disabled'       	=> false,
	    'group'          	=> '',
	    'hint'           	=> '',
	    'id'             	=> 'image',
	    'label'         	=> '',
	    'labelclass'    	=> '',
	    'multiple'       	=> false,
	    'name'           	=> '',
	    'onchange'       	=> '',
	    'onclick'        	=> '',
	    'pattern'        	=> '',
	    'readonly'       	=> false,
	    'repeat'         	=> false,
	    'required'       	=> false,
	    'size'           	=> 240,
	    'spellcheck'     	=> false,
	    'validate'       	=> '',
	    'value'          	=> '',
	    'checkedOptions' 	=> [],
	    'hasValue'       	=> true,
	    'options'        	=> [],
	    'preview'        	=> 'tooltip',
	    'previewHeight'  	=> 200,
	    'previewWidth'   	=> 200,
	    'asset'          	=> '',
		'authorField'    	=> '',
		'authorId'       	=> '',
	    'folder'         	=> '',
	    'link'           	=> ''
	];

	public static function make()
	{
		return new Media;
	}

	public function render()
	{
		return JLayoutHelper::render('joomla.form.field.media', $this->options);
	}
}