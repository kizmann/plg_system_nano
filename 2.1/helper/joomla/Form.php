<?php

namespace Nano\Helper\Joomla;

use Nano\Helper\Joomla\Options as Options;
use JHtml as JHtml;

class Form extends Options
{
	public $options = [
		'form' 		=> 'adminForm',
		'options' 	=> []
	];

	public static function make()
	{
		return new Form;
	}

	public function render()
	{
		return JHtml::_('searchtools.form', ...array_values($this->options));
	}
}
