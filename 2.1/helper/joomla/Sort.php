<?php

namespace Nano\Helper\Joomla;

use Nano\Helper\Joomla\Options as Options;
use JHtml as JHtml;

class Sort extends Options
{
	public $options = [
		'title' 		=> '',
		'order' 		=> '',
		'direction' 	=> 'asc',
		'selected' 		=> '',
		'task' 			=> 'index',
		'newDirection' 	=> 'asc',
		'tooltip' 		=> ''
	];

	public static function make()
	{
		return new Sort;
	}

	public function render()
	{
		return JHtml::_('grid.sort', ...array_values($this->options));
	}
}
