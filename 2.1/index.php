<?php

define('NANO_ROOT', '/plugins/system/nano/2.1/');
define('NANO_VERSION', 2.1);

$baseDir = rtrim(JPATH_ROOT, '/') . '/' . trim(NANO_ROOT, '/');

require_once($baseDir . '/vendor/autoload.php');
require_once($baseDir . '/component/Loader.php');

$loader = (new Nano\Component\Loader)->addPrefix('Nano\\', $baseDir)->register();

require_once($baseDir . '/shorthand/Helper.php');
require_once($baseDir . '/shorthand/Types.php');
require_once($baseDir . '/shorthand/Joomla.php');
require_once($baseDir . '/shorthand/Nano.php');
require_once($baseDir . '/shorthand/Alias.php');
require_once($baseDir . '/shorthand/Menu.php');
require_once($baseDir . '/boot.php');