<?php

namespace Nano\Component;

use Illuminate\Support\Collection as Collection;

class Provider
{

	protected static $services;

	public static function init()
	{
		self::$services = self::$services instanceof Collection ? self::$services : new Collection([]);
	}

	public static function register($index, $callback)
	{
		self::$services->push(['index' => $index, 'callback' => $callback]);
	}

	public static function boot($instance)
	{
		self::$services->map(function($service) use ($instance) {
			$instance->{$service['index']} = $service['callback']();
		});
	}

}