<?php

namespace Nano\Component;

use LaravelArdent\Ardent\Ardent as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Model extends Eloquent
{

	public function __construct($attributes = [])
	{
		$this->syncOriginal();
		parent::__construct($attributes);
	}

}