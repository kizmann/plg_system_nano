<?php

namespace Nano\Component;

use Exception as Exception;
use JFactory as JFactory;
use JText as JText;

class Factory
{

	protected $type;
	protected $namespace;
	protected $controller;
	protected $method;

	public function prepare($value, $ucfirst = true, $strtolower = true, $trim = false)
	{	
		if ( $trim ) {
			$value = trim($value, 'com_');
		}

		if ( $strtolower ) {
			$value = strtolower($value);
		}

		if ( $ucfirst ) {
			$value = ucfirst($value);
		}

		return $value;
	}

	public function type()
	{
		return $this->prepare(app()->isSite() ? 'site' : 'admin');
	}

	public function method()
	{
		return $this->prepare(request()->method());
	}

	public function namespace($default = '', $option = 'option')
	{
		return $this->prepare(request()->get($option, $default), true, true, true);
	}

	public function controller($default = 'index', $option = 'view')
	{
		return $this->prepare(request()->get($option, $default));
	}

	public function action($default = 'index', $option = 'task')
	{
		return $this->prepare(request()->get($option, $default));
	}

	protected function build($ajax = false, $namespace = [], $controller = [], $action = [])
	{
		$class = $this->namespace(...$namespace) . '\\' . $this->prepare('controller') . '\\' . ($ajax ? $this->prepare('ajax') : $this->type()) . '\\' . $this->controller(...$controller);

		if ( ! class_exists($class) ) {
			throw new Exception('Class "' . $class . '" does not exists!');
		}

		$action = $this->action(...$action);

		if ( method_exists($class, $action . $this->method()) ) {
			$action = $action . $this->method();
		}

		return call_user_func([(new $class), $action]);
	}

	public function ajax($namespace = [], $controller = [], $action = [])
	{
		try {
			$this->build(true, $namespace, $controller, $action);
		} catch (Exception $exception) {

			$message = $exception->getMessage();

			if ( ! is_string($message) ) {
				$message = 'Unexpected error';
			}

			$code = $exception->getCode();

			if ( ! is_int($code) || $code === 0 ) {
				$code = 500;
			}

			view()->json(['message' => $message], $code);
		}
	}

	public function http($namespace = [], $controller = [], $action = [])
	{
		$this->build(false, $namespace, $controller, $action);
	}

	public function boot($namespace = [], $controller = [], $action = [])
	{
		if ( request()->ajax() ) {
			return $this->ajax($namespace, $controller, $action);
		}

		return $this->http($namespace, $controller, $action);
	}

}