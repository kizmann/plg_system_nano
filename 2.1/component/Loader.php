<?php

namespace Nano\Component;

use Symfony\Component\ClassLoader\Psr4ClassLoader as ClassLoader;

class Loader extends ClassLoader
{

	private $prefixes = [];

	public function findFile($class)
    {
        $class = ltrim($class, '\\');

        foreach ($this->prefixes as list($currentPrefix, $currentBaseDir)) {

        	if ( 0 !== strpos($class, $currentPrefix) ) {
        		continue;
        	}

        	$classWithoutPrefix = substr($class, strlen($currentPrefix));
        	$classExploded = explode('\\', $classWithoutPrefix); $classExplodedLower = explode('\\', strtolower($classWithoutPrefix));

        	$file = $currentBaseDir . implode(DIRECTORY_SEPARATOR, array_slice($classExplodedLower, 0, -1)) . DIRECTORY_SEPARATOR . end($classExploded) . '.php';

        	if ( ! file_exists($file) ) {
        		continue;
        	}

        	return $file;
        }
    }

	public function addPrefix($prefix, $baseDir)
	{
		$prefix = trim($prefix, '\\').'\\'; $baseDir = rtrim($baseDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->prefixes[] = array($prefix, $baseDir);
		return $this;
	}

	public function register($prepend = true)
	{
		spl_autoload_register(array($this, 'loadClass'), true, $prepend);
		return $this;
	}


	public function unregister()
	{
		spl_autoload_unregister(array($this, 'loadClass'));
		return $this;
	}

}