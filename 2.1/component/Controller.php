<?php

namespace Nano\Component;

use Nano\Component\Provider as Provider;
use Exception as Exception;
use JText as JText;

class Controller
{

	public function __construct()
	{
		Provider::boot($this);
	}

	public function __call($method, $vars)
	{
		$this->beforeCall($method, $vars);
		throw new Exception('Method "' . $method . '" does not exists!');
	}

	protected function beforeCall($method, $vars)
	{
		// Run code before error has been thrown
	}

}