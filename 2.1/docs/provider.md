# Provider

### Register Service

```php
Provider::register('config', function() {
	return new MyService;
});
```

### Use Service
```php
/**
 * Filepath: "/administrator/components/com_componentname/controller/admin/helloworld.php"
 */
 
 $this->config->doWhatever();