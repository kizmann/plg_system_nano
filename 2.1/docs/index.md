## Create a Component

```php
<?php

	/**
	 * Filepath: "/administrator/components/com_componentname/componentname.php"
	 */

	if( ! defined('NANO_VERSION') || NANO_VERSION < 2 ) {
	    throw new Exception('Nano Framework version 2.x is required.');
	}

	use Nano\Component\Loader as Loader;
	use Nano\Component\Provider as Provider;
	use Nano\Component\Factory as Factory;

	use ComponentName\Service\MyCustomHelper as MyCustomHelper;

	include_once('install/tablename.php');

	(new Loader)->addPrefix('ComponentName', JPATH_COMPONENT_ADMINISTRATOR)->register();
	(new Factory)->boot();
```
