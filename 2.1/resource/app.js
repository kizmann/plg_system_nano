window.locale.init('en'); window.i18n = new window.locale.i18n;
window.VueData = {}, window.VueComponents = {}, window.VueDirectives = {}, window.VueTransitions = {};

jQuery(function() {

    Vue.config.productionTip = false;

    var custom = {
        data: window.VueData, directives: window.VueDirectives, transitions: window.VueTransitions
    };

    jQuery.each(window.VueComponents, function(index, component) {
    	Vue.component(index, component);
    });

    var options = {
        el: '[data-nano]',
    };

    var merged = jQuery.extend(true, {}, options, custom);

    window.Vue = new Vue(merged);

});