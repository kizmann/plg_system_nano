<?php

use Illuminate\Database\Capsule\Manager as DB;

class MenuBuilder {

    public $route;

    public function __construct($route = '')
    {
        $this->route = $route;
    }

    public static function make($route = '')
    {
        return new MenuBuilder($route);
    }

    public function getID()
    {
        $match = preg_match('/(^[0-9]+$|Itemid=[0-9]+)/', $this->route, $id);

        if ( ! $match ) {
            return;
        }

        return ltrim($id[1], 'Itemid=');
    }

    public function getLink()
    {
        $match = preg_match('/^(index\.php|#)/', $this->route, $link);

        if ( ! $match ) {
            return;
        }

        return $this->route;
    }

    public function getMenuById($id)
    {
        return DB::table('menu')->where('id', $id)->first();
    }

    public function getMenuByLink($link)
    {
        return DB::table('menu')->where('link', ltrim($link, '/'))->first();
    }

    public function build($menu = null)
    {
        $id = $this->getID();
        
        if ( $id ) {
            $menu = $this->getMenuById($id);
        }

        $link = $this->getLink();

        if ( $link ) {
            $menu = $this->getMenuByLink($link);
        }

        if ( ! $menu ) {
            return $this->route;
        }

        if ( $menu->type === 'url' ) {
            return JRoute::_('index.php?Itemid=' . $menu->parent_id . $menu->link);
        } 

        if ( $menu->type === 'alias' ) {
            return JRoute::_('index.php?Itemid=' . json_decode($menu->params)->aliasoptions);
        } 

        return JRoute::_('index.php?Itemid=' . $menu->id);
    }

}

function menu($route)
{
    return MenuBuilder::make($route)->build();
}