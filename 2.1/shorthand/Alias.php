<?php

class_alias(
	'Nano\Helper\Html\Node', 'NNode'
);

class_alias(
	'Nano\Helper\Html\Form', 'NForm'
);

class_alias(
	'Nano\Helper\Html\Button', 'NButton'
);

class_alias(
	'Nano\Helper\Html\Label', 'NLabel'
);

class_alias(
	'Nano\Helper\Html\Input', 'NInput'
);

class_alias(
	'Nano\Helper\Html\Textarea', 'NTextarea'
);

class_alias(
	'Nano\Helper\Html\Select', 'NSelect'
);

class_alias(
	'Nano\Helper\Html\Radio', 'NRadio'
);

class_alias(
	'Nano\Helper\Joomla\Editor', 'NJEditor'
);

class_alias(
	'Nano\Helper\Joomla\Calendar', 'NJCalendar'
);

class_alias(
	'Nano\Helper\Joomla\Media', 'NJMedia'
);

class_alias(
	'Nano\Helper\Joomla\Form', 'NJForm'
);

class_alias(
	'Nano\Helper\Joomla\Sort', 'NJSort'
);