<?php

function request()
{
	return forward_static_call_array( ['Nano\\Service\\Request', 'make'], func_get_args() );
}

function state()
{
	return forward_static_call_array( ['Nano\\Service\\State', 'make'], func_get_args() );
}

function view()
{
	return forward_static_call_array( ['Nano\\Service\\View', 'make'], func_get_args() );
}

function toolbar()
{
	return forward_static_call_array( ['Nano\\Joomla\\Toolbar', 'make'], func_get_args() );
}

function sidebar()
{
	return forward_static_call_array( ['Nano\\Joomla\\Sidebar', 'make'], func_get_args() );
}

function pagination()
{
	return forward_static_call_array( ['Nano\\Joomla\\Pagination', 'make'], func_get_args() );
}

function node()
{
	return forward_static_call_array( ['Nano\\Helper\\Html\\Node', 'make'], func_get_args() );
}