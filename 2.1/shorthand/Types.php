<?php

function toString($value, $default = '') {
	
	if ( isset($value) === false || empty($value) === true || is_string($value) === false ) {
		return $default;
	}

	return $value;
}

function toBoolean($value, $default = false) {

	if ( isset($value) === false || empty($value) === true ) {
		return $default;
	}

	if ( is_string($value) === true && $value === 'false' ) {
		return false;
	}

	return true;
}

function toFloat($value, $default = 0) {

	if ( isset($value) === false || empty($value) === true ) {
		return $default;
	}
	
	if ( is_string($value) === true ) {
		return floatval($value);
	}

	return $value;
}

function toInteger($value, $default = 0) {

	if ( isset($value) === false || empty($value) === true ) {
		return $default;
	}

	if ( is_string($value) === true ) {
		return intval($value);
	}

	return $value;
}

function toArray($value, $default = []) {

	if ( isset($value) === false || empty($value) === true ) {
		return $default;
	}

	if ( is_string($value) === true || is_numeric($value) === true ) {
		return [$value];
	}

	return $value;
}

function toObject($value, $default = '{}')
{
    if ( isset($value) === false || empty($value) === true ) {
		return json_encode($default);
	}

	return json_decode(json_encode($value));
}

function getArrayAttribute($array, $key, $default = null)
{
    if ( ! $array ) {
        return $default;
    }

    return $array[$key];
}

function getObjectAttribute($object, $key, $default = null)
{
    if ( ! $object ) {
        return $default;
    }

    return $object->$key;
}