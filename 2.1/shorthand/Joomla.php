<?php

function trans()
{
	return forward_static_call_array( ['JText', 'sprintf'], func_get_args() );
}

function route()
{
	return forward_static_call_array( ['JRoute', '_'], func_get_args() );
}

function app()
{
	return forward_static_call_array( ['JFactory', 'getApplication'], func_get_args() );
}

function html()
{
	return forward_static_call_array( ['JHtml', '_'], func_get_args() );
}

function doc()
{
	return forward_static_call_array( ['JFactory', 'getDocument'], func_get_args() );
}

function config()
{
	return forward_static_call_array( ['JFactory', 'getConfig'], func_get_args() );
}

function mailer()
{
	return forward_static_call_array( ['JFactory', 'getMailer'], func_get_args() );
}

function lang()
{
	return forward_static_call_array( ['JFactory', 'getLanguage'], func_get_args() );
}

function user()
{
	return forward_static_call_array( ['JFactory', 'getUser'], func_get_args() );
}

function csrf()
{
	return forward_static_call_array( ['JSession', 'checkToken'], func_get_args() );
}

function token()
{
	return forward_static_call_array( ['JSession', 'getFormToken'], func_get_args() );
}

function rootURL()
{
	return forward_static_call_array( ['JURI', 'root'], func_get_args() );
}

function baseURL()
{
	return forward_static_call_array( ['JURI', 'base'], func_get_args() );
}

function currentURL()
{
	return forward_static_call_array( ['JURI', 'getInstance'], func_get_args() );
}