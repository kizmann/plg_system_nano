<?php

namespace Nano\Joomla;

use JFactory as JFactory;
use JToolBarHelper as JToolBarHelper;

class Toolbar {

    public static function make()
    {
        return new Toolbar;
    }

    protected function call($class, $method, $arguments)
    {
        if( ! $class ) {
            $class = 'JToolBarHelper';
        }

        $call = forward_static_call_array( array($class, $method), $arguments );

        return $call;
    }

    public function title()
    {
        $this->call( null, 'title', func_get_args() );
        return $this;
    }

    public function addNew()
    {
        $this->call( null, 'addNew', func_get_args() );
        return $this;
    }

    public function back()
    {
        $this->call( null, 'back', func_get_args() );
        return $this;
    }

    public function custom()
    {
        $this->call( null, 'custom', func_get_args() );
        return $this;
    }

    public function link()
    {
        $this->call( null, 'link', func_get_args() );
        return $this;
    }

    public function media()
    {
        $this->call( null, 'media_manager', func_get_args() );
        return $this;
    }

    public function deleteList()
    {
        $this->call( null, 'deleteList', func_get_args() );
        return $this;
    }

    public function publish()
    {
        $this->call( null, 'publish', func_get_args() );
        return $this;
    }

    public function unpublish()
    {
        $this->call( null, 'unpublish', func_get_args() );
        return $this;
    }

    public function trash()
    {
        $this->call( null, 'trash', func_get_args() );
        return $this;
    }

    public function apply()
    {
        $this->call( null, 'apply', func_get_args() );
        return $this;
    }

    public function save()
    {
        $this->call( null, 'save', func_get_args() );
        return $this;
    }

    public function save2new()
    {
        $this->call( null, 'save2new', func_get_args() );
        return $this;
    }

    public function save2copy()
    {
        $this->call( null, 'save2copy', func_get_args() );
        return $this;
    }

    public function cancel()
    {
        $this->call( null, 'cancel', func_get_args() );
        return $this;
    }

    public function preferences()
    {
        $this->call( null, 'preferences', func_get_args() );
        return $this;
    }

    public function modal()
    {
        $this->call( null, 'modal', func_get_args() );
        return $this;
    }

    public function disable()
    {
        JFactory::getApplication()->input->set('hidemainmenu', true);
        return $this;
    }

}

function toolbar() {
    return forward_static_call_array( ['Toolbar', 'make'], func_get_args() );
}