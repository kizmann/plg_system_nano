<?php

namespace Nano\Joomla;

use JHtmlSidebar as JHtmlSidebar;

class Sidebar
{

    public static function make()
    {
        return new Sidebar;
    }

    protected function call($class, $method, $arguments)
    {
        if( ! $class ) {
            $class = 'JHtmlSidebar';
        }

        $call = forward_static_call_array( array($class, $method), $arguments );

        return $call;
    }

    public function entry()
    {
        $this->call( null, 'addEntry', func_get_args() );
        return $this;
    }

    public function filter()
    {
        $this->call( null, 'addFilter', func_get_args() );
        return $this;
    }

    public function render()
    {
        $call = $this->call( null, 'render', func_get_args() );
        return $call;
    }

}

function sidebar() {
    return forward_static_call_array( ['Sidebar', 'make'], func_get_args() );
}