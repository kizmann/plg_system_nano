<?php

namespace Nano\Joomla;

use JPagination as JPagination;

class Pagination
{

    protected $collection;
    protected $pagination;

    public static function make($collection, $limitstart, $limit)
    {
        return new Pagination($collection, $limitstart, $limit);
    }

    public function __construct($collection, $limitstart, $limit)
    {
        $this->collection = $collection;
        $this->pagination = new JPagination($collection->count(), $limitstart, $limit);
    }

    public function slice()
    {
        return $this->collection->slice($this->pagination->limitstart, $this->pagination->limit)->values();
    }

    public function pages()
    {
        return $this->pagination->getPagesLinks();
    }

    public function total()
    {
        return $this->pagination->total;
    }

}

function pagination() {
    return forward_static_call_array( ['Pagination', 'make'], func_get_args() );
}
