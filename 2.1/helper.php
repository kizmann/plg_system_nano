<?php

function array_assign($array, $keys, $value, $delimiter = '.')
{
	$keys = explode($delimiter, $keys);

	foreach ( $keys as $key ) {

		if ( ! isset($array[$key]) ) {
			$array[$key] = [];
		}

		$array = &$array[$key];
	}

	$array = $value;
}

function extend($array, $indexLabel = 'label', $indexValue = 'value', $value = '', $label = 'jselect')
{
	return array_merge([[$indexValue => $value, $indexLabel => $label]], $array);
}




