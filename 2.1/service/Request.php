<?php
namespace Nano\Service;

use Illuminate\Http\Request as IlluminateRequest;
use JRequest as JRequest;

class Request extends IlluminateRequest
{

    public static function make()
    {
        return (new Request)->initJoomlaRequest();
    }

    public function initJoomlaRequest()
    {
        $this->initialize(JRequest::get('get'), JRequest::get('post'), [], JRequest::get('cookie'), JRequest::get('files'), JRequest::get('server'));
        return $this;
    }

}