<?php

namespace Nano\Service;

use JFactory;

class State
{

	public static function make()
	{
		return new State;
	}

	public function __construct($prefix = '')
	{
		$app = JFactory::getApplication();

		$this->prefix = $prefix === '' ? $app->input->get('option') : $prefix;
	}

	public function get($key, $default = null)
	{
		$app = JFactory::getApplication();

		return $app->getUserState($this->prefix . '.' . $key, $default);
	}

	public function set($key, $value)
	{
		$app = JFactory::getApplication();

		return $app->setUserState($this->prefix . '.' . $key, $value);
	}

	public function input($key, $input, $default)
	{
		$app = JFactory::getApplication();

		return $app->getUserStateFromRequest($this->prefix . '.' . $key, $input, $default);
	}

}