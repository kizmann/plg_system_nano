<?php
namespace Nano\Service;

use Symfony\Component\HttpFoundation\Response as Response;
use Symfony\Component\HttpFoundation\JsonResponse as JsonResponse;
use Nano\Component\Provider as Provider;
use JFactory as JFactory;

class View
{

    public static function make()
    {
        return new View;
    }

	public function csv($vars)
	{
		header('Content-type: text/csv');
		header('Content-Disposition: attachment');

		$charset = mb_detect_encoding($vars, 'UTF-8, ISO-8859-1, ISO-8859-15', true);
		echo mb_convert_encoding($vars, 'Windows-1252', $charset);

		JFactory::getApplication()->close();
	}

	public function json($data = [], $code = 200, $headers = [])
	{
		$response = new JsonResponse($data, $code, $headers);
		$response->send();
		app()->close();
	}

    public function render($template, $vars, $admin = false)
    {
    	call_user_func(function() use ($template, $vars, $admin) {
    		extract($vars); unset($vars); $provider = new \StdClass; Provider::boot($provider);
    		include(($admin ? JPATH_COMPONENT_ADMINISTRATOR : JPATH_COMPONENT)  . '/' . 'view' . '/' . ltrim($template, '/') . '.php');
    	});
    }

}